import validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput(data) {
	let errors = {};
	if(validator.isEmpty(data.email)) {
		errors.email = 'Email is required';
	}
	if(!validator.isEmail(data.email)) {
		errors.email = 'Email is not valid';
	}
	if(validator.isEmpty(data.password)) {
		errors.password = 'Password is required';
	}
	if(validator.isEmpty(data.mobile)) {
		errors.mobile = 'Mobile is required';
	}
	if(!validator.isNumeric(data.mobile)) {
		errors.mobile = 'Mobile no is not valid';
	}
	if(validator.isEmpty(data.registration)) {
		errors.registration = 'Registration no is required';
	}
	if(validator.isEmpty(data.make)) {
		errors.make = 'Make is required';
	}
	if(validator.isEmpty(data.model)) {
		errors.model = 'Model is required';
	}
	if(validator.isEmpty(data.service)) {
		errors.service = 'Service Type is required';
	}

	return {
		errors,
		isValid: isEmpty(errors)
	}
}