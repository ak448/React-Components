import validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateLogin(data) {
	let errors = {};
	if(validator.isEmpty(data.email)) {
		errors.email = 'Email is required';
	}
	if(!validator.isEmail(data.email)) {
		errors.email = 'Email is not valid';
	}
	if(validator.isEmpty(data.password)) {
		errors.password = 'Password is required';
	}
	return {
		errors,
		isValid: isEmpty(errors)
	}
}