import React, { Component } from 'react';
import { 
    AppRegistry, 
    Text, 
    View, 
    StyleSheet, 
    TouchableOpacity,
 } from 'react-native';

  export default class StartUp extends Component {
      render() {
          return (
              <View style={styles.note}>

                <Text style={styles.noteText}>{this.props.val.date}</Text>
                <Text style={styles.noteText}>{this.props.val.note}</Text>  

                <TouchableOpacity onPress={this.props.deleteMethod} style={styles.noteDelete}>
                  <Text style={styles.noteDeleteText}>D</Text>
                </TouchableOpacity>

              </View>
        );
    }
  }

  const styles = StyleSheet.create({
     note: {
          position: 'relative',
          padding: 20,
          paddingRight: 100,
          borderBottomWidth: 2,
          borderBottomColor:  '#ededed',
     },
     noteText: {
          paddingLeft: 20,
          borderLeftWidth: 10,
          borderLeftColor: '#ededed',
     },
     noteDelete: {
          position: 'absolute',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#2980b9',
          padding: 10,
          top: 10,
          bottom: 10,
          right: 10,
     },
     noteDeleteText: {
          color: 'white',
     } 
  });

