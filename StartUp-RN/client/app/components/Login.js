import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  Platform,
  ActivityIndicator
} from 'react-native';
import axios from 'axios';
import InputField from './common/InputField';
import Button from './common/Button';
import validateLogin from './validation/Login';

const host = Platform.select({
    ios: 'localhost',
    android: '10.0.2.2',
});

class Login extends Component {
	static navigationOptions = {
		title: 'Login'
	}
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	email: '',
	  	password: '',
	  	errors: '',
	  	errorsMsg: '',
	  	animating: false
	  };
	  this.changeEmail = this.changeEmail.bind(this);
	  this.changePassword = this.changePassword.bind(this);
	  this.login = this.login.bind(this);
	}
	changeEmail = (value) => {
		this.setState({
			email: value
		});
	}
	changePassword = (value) => {
		this.setState({
			password: value
		});
	}
	isValid = () => {
		const {errors, isValid} = validateLogin(this.state);
		if(!isValid) {
			this.setState({errors});
		}
		return isValid;
	}
	login = () => {
		if(this.isValid()) {
			this.setState({errors:{}, animating: true});
			axios.post(`http://${host}:3000/login`, this.state)
				.then(function(user){
					console.log(user.data);
					if(user.data.status === true) {
						this.setState({errorsMsg: user.data.msg, animating: false});
						this.props.navigation.navigate('Dashboard');
					} else {
						this.setState({errorsMsg: user.data.msg, animating: false});
					}
				}.bind(this))
				.catch(function(err){
					console.log(err);
				});
		}
	}
	renderButton() {
		if(this.state.animating === false) {
			return (<View style={{margin: 10}}>
      				<Button onPress={this.login}>LOGIN</Button>
      				</View>);
		} else {
			return (<ActivityIndicator
               animating = {this.state.animating}
               color = '#bc2b78'
               size = "large"
               style = {styles.activityIndicator}
         />);
		}
	}
  render() {
  	const {errors} = this.state;
    return (
      <View >
      	
      	<InputField 
      		placeholder="Email"
      		onChangeFunc={this.changeEmail}
      		value={this.state.email}
      		keyboard="email-address"
      	/>
      	<Text style={{color: 'red',marginLeft: 25}}>{errors.email}</Text>

      	<InputField 
      		placeholder="Password"
      		onChangeFunc={this.changePassword}
      		value={this.state.password}
      		keyboard="default"
      		secureTextEntry={true}
      	/>
      	<Text style={{color: 'red',marginLeft: 25}}>{errors.password}</Text>

      	<Text style={{color: 'red',marginLeft: 25}}>{this.state.errorsMsg}</Text>
      	{this.renderButton()}
      	
      </View>
    );
  }
}

const styles = StyleSheet.create({

});


export default Login;