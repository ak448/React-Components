import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  ScrollView,
  Picker,
  Text,
  Platform
} from 'react-native';
import axios from 'axios';
import InputField from './common/InputField';
import Button from './common/Button';
import validateInput from './validation/Register';

const host = Platform.select({
    ios: 'localhost',
    android: '10.0.2.2',
});

class Register extends Component {
	static navigationOptions = {
		title: 'Registration'
	}
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	email: '',
	  	password: '',
	  	mobile: '',
	  	registration: '',
	  	makes: [],
	  	make: '',
	  	models: [],
	  	model: '',
	  	services: ['Free', 'Paid', 'Accident Repair', 'Running Repair'],
	  	service: '',
	  	errors: ''
	  };
	  this.changeEmail = this.changeEmail.bind(this);
	  this.changePassword = this.changePassword.bind(this);
	  this.changeMobile = this.changeMobile.bind(this);
	  this.changeRegistration = this.changeRegistration.bind(this);
	  this.onRegister = this.onRegister.bind(this);
	}
	
	componentDidMount = () => {
		console.log('did');
		this.getMakes();
	}
	getMakes = () => {                     //// getting makes 
		axios.get(`http://${host}:3000/make`)
		.then(function(makes){
			console.log(makes.data);
			this.setState({
				makes: makes.data
			});
		}.bind(this))
		.catch(function(error){
			console.log(error);
		});
	}
	setMake = (itemValue, itemIndex) => {       /////setting makes to car make
		console.log(itemValue);
		this.setState({
			make: itemValue
		});
		this.getModels(itemValue);
	}
	getModels = (makeId) => {
		console.log(`http://${host}:3000/models/${makeId}`);
		axios.get(`http://${host}:3000/models/`+makeId)
		 .then(function(models){
		 	console.log(models.data);
		 	this.setState({
		 		models: models.data
		 	});
		 }.bind(this))
		 .catch(function(error){

		 });
	}
	isValid = () => {
		console.log(this.state);
		const {errors,isValid} = validateInput(this.state);
		if(!isValid){
			this.setState({errors});
		}
		return isValid;
	}
	onRegister = () => {
		console.log('render validation');
		if(this.isValid()) {
			this.setState({errors: {}});
			axios.post(`http://${host}:3000/register`, this.state)
				.then(function(user){
					console.log(user.data);
					this.props.navigation.navigate('Dashboard');
				})
				.catch(function(err){
					this.setState({errors: err});
				});
		}
	}
	changeEmail = (value) => {
		this.setState({
			email: value
		});
	}
	changePassword = (value) => {
		this.setState({
			password: value
		});
	}
	changeMobile = (value) => {
		this.setState({
			mobile: value
		});
	}
	changeRegistration = (value) => {
		this.setState({
			registration: value
		});
	}
	onChangeSelect = (value) => {
		console.log('change');
	}
  render() {
  	const {errors} = this.state;
  	console.log('errors', errors);
  	let makeItems = this.state.makes.map( (s, i) => {
            return <Picker.Item key={i} value={s._id} label={s.name} />
        });
     let modelItems = this.state.models.map( (s, i) => {
            return <Picker.Item key={i} value={s.name} label={s.name} />
        });
     let serviceItems = this.state.services.map( (s, i) => {
            return <Picker.Item key={i} value={s} label={s} />
        });    
    return (
      <ScrollView>
      <InputField 
      		placeholder="Email"
      		onChangeFunc={this.changeEmail}
      		value={this.state.email}
      		keyboard="email-address"
      	/>
      	<Text style={{color: 'red',marginLeft: 25}}>{errors.email}</Text>
      	<InputField 
      		placeholder="Password"
      		onChangeFunc={this.changePassword}
      		value={this.state.password}
      		keyboard="default"
      		secureTextEntry={true}
      	/>
      	<Text style={{color: 'red',marginLeft: 25}}>{errors.password}</Text>
      	<InputField 
      		placeholder="Mobile"
      		onChangeFunc={this.changeMobile}
      		value={this.state.mobile}
      		keyboard="numeric"
      	/>
      	<Text style={{color: 'red',marginLeft: 25}}>{errors.mobile}</Text>
      	<View style={styles.selectBox}>
	      	<View style={{width: 150, padding: 15}}><Text>Car Make</Text></View>
	      	<View style={{width: 180}}><Picker
			   selectedValue={this.state.make}
			   onValueChange={(itemValue, itemIndex) => this.setMake(itemValue, itemIndex)}
			   
			>
			{makeItems}
			</Picker>
			</View>
		</View>
		<Text style={{color: 'red',marginLeft: 25}}>{errors.make}</Text>
		<View style={styles.selectBox}>
	      	<View style={{width: 150, padding: 15}}><Text>Car Model</Text></View>
	      	<View style={{width: 180}}><Picker
			   selectedValue={this.state.model}
			   onValueChange={(itemValue, itemIndex) => this.setState({model: itemValue})}
			   
			>
			{modelItems}
			</Picker>
			</View>
		</View>
		<Text style={{color: 'red',marginLeft: 25}}>{errors.model}</Text>
		<View style={styles.selectBox}>
	      	<View style={{width: 150, padding: 15}}><Text>Service Type</Text></View>
	      	<View style={{width: 180}}><Picker
			   selectedValue={this.state.service}
			   onValueChange={(itemValue, itemIndex) => this.setState({service: itemValue})}
			   
			>
			{serviceItems}
			</Picker>
			</View>
		</View>
		<Text style={{color: 'red',marginLeft: 25}}>{errors.service}</Text>
		<InputField 
      		placeholder="Registration"
      		onChangeFunc={this.changeRegistration}
      		value={this.state.registration}
      		keyboard="default"
      	/>
      	<Text style={{color: 'red',marginLeft: 25}}>{errors.registration}</Text>
      	<View style={{margin: 10}}><Button onPress={this.onRegister}>REGISTER</Button></View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
	selectBox: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-start',
		//alignItems: 'flex-start',
		marginRight: 15, 
		marginLeft: 15,
		
	}
});


export default Register;