import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Picker
} from 'react-native';

class SelectBox extends Component {
	constructor(props) {
	  super(props);
	
	  this.state = {};
	}
  render() {
  	console.log(this.props.selectedValue);
  	let serviceItems = this.props.selectedValue.map( (s, i) => {
            return <Picker.Item key={i} value={s} label={s} />
        });

    return (
      <View>
      	<Picker
		   selectedValue={this.props.selectedValue}
		   style={styles.selectBox}
		   onValueChange={(itemValue, itemIndex) => this.setState({make: itemValue})}>
		   {serviceItems}
		</Picker>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	selectBox: {
		marginRight: 15, 
		marginLeft: 15
	}
});


export default SelectBox;