import React, { Component } from 'react';
import { 
    Text, 
    Image, 
    View, 
    StyleSheet, 
    ScrollView,
    TextInput,
    TouchableOpacity,
 } from 'react-native';

 import Button from './components/common/Button';

  export default class App extends Component {
      static navigationOptions = {
        header: false
      } 
      render() {
          const {navigate} = this.props.navigation;
         return (
              <View style={styles.container}>

                  <View style={styles.header}>
                      <Text style={styles.headerText}>TEST</Text>
                  </View>

                  <ScrollView style={styles.scrollContainer}>
                      <View style={{margin: 10}}><Button onPress={()=>navigate('Register')}>Register</Button></View>
                      <View style={{margin: 10}}><Button onPress={()=>navigate('Login')}>Login</Button></View>
                  </ScrollView>
                  

              </View>
        );
    }
}

  const styles = StyleSheet.create({
      container:{
        flex:1,
      },
      header:{
        backgroundColor:'#E91E63',
        alignItems:'center',
        justifyContent:'center',
        borderBottomWidth:10,
        borderBottomColor:'#ddd',
      },
      headerText:{
        color:'white',
        fontSize:18,
        padding:26,
      },
      scrollContainer: {
        flex: 1,
        paddingTop: 150
      },
      footer: {
        position: 'absolute',
        alignItems: 'center',
        bottom: 0,
        left: 0,
        right: 0,
      },
      addButton: {
        backgroundColor: '#E91E63',
        width: 90,
        height:90,
        borderRadius: 50,
        borderColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
        marginBottom: -45,
        zIndex: 10,
      },
      addButtonText: {
        color: '#fff',
        fontSize: 24,
      },
      textInput: {
        alignSelf: 'stretch',
        color: '#fff',
        padding: 20,
        paddingTop: 46,
        backgroundColor: '#252525',
        borderTopWidth: 2,
        borderTopColor: '#ededed',
      }
  });

