import React, { Component } from 'react';
import { 
    AppRegistry, 
 } from 'react-native';

 import {StackNavigator} from 'react-navigation';
 import App from './app/App';
 import Login from './app/components/Login';
 import Register from './app/components/Register';
 import Dashboard from './app/components/Dashboard';

 const Route = new StackNavigator({
 	Home: {screen: App},
 	Login: {screen: Login},
 	Register: {screen: Register},
 	Dashboard: {screen: Dashboard}
 });

AppRegistry.registerComponent('TestRN', () => Route);