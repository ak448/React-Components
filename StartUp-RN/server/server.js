var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var db = mongoose.connect('mongodb://localhost/testrn-db');

var Make = require('./model/make');
var Model = require('./model/model');
var Register = require('./model/register');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(function(req, res, next) {
 res.setHeader('Access-Control-Allow-Origin', '*');
 res.setHeader('Access-Control-Allow-Credentials', 'true');
 res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
 res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
 res.setHeader('Cache-Control', 'no-cache');
 next();
});

app.post('/login', function(req, res){
	var loginData = new Register();
	var usrEmail = req.body.email;
	var password = req.body.password;
	Register.findOne({email: usrEmail}, function(err, user){
		if (!user) {
      res.send({status: false, msg: 'Authentication failed. User not found.'});
    } else {
    	if(user.password === password) {
    		res.send({status: true});
    	} else {
    		res.send({status: false, msg: 'Authentication failed. Wrong password.'});
    	}
    }
	});
});

app.get('/make', function(req, res){     ////getting makes
	Make.find({}, function(err, makes){
		if(err){
			res.send(err);
		} else {
			res.send(makes);
		}
	});
	
});

app.post('/make', function(req, res){     /////saving makes
	var makeData = new Make();
	makeData.name = req.body.name;
	makeData.save(function(err, make){
		if(err){
			res.send(err);
		} else {
			res.send(make);
		}
		
	});
});

app.post('/model', function(req, res){     /////saving models
	var modelData = new Model();
	modelData.name = req.body.name;
	modelData.makeId = req.body.makeId;
	modelData.save(function(err, models){
		if(err){
			res.send(err);
		} else {
			res.send(models);
		}
		
	});
});

app.get('/models/:makeId', function(req, res){     /////getting models by make id
	var makeId = req.params.makeId;
	Model.find({makeId: makeId}).populate({path: 'makeId', Model: 'Make'}).exec(function(err, models){
		if(err){
			res.send(err);
		} else {
			res.send(models);
		}
		
	});
});

app.post('/register', function(req, res){
	var userData = new Register();
	userData.email = req.body.email;
	userData.password = req.body.password;
	userData.mobile = req.body.mobile;
	userData.make = req.body.make;
	userData.model = req.body.model;
	userData.service = req.body.service;
	userData.registration = req.body.registration;
	userData.save(function(err, user){
		if(err){
			res.send(err);
		} else {
			res.send(user);
		}
	});
});

app.listen(3000, function() {
	console.log('Server running at port 3000');
});
