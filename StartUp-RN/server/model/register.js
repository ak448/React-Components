var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var registerUser = new Schema({
	email: {type: String, unique: true},
	password: String,
	mobile: String,
	make: String,
	model: String,
	service: String,
	registration: String,
	status: {type: Boolean, default: true},
	created_at: {type: Date, default: new Date()}
});

module.exports = mongoose.model('Register', registerUser);