var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var modelSchema = new Schema({
	name: { type: String, required: [true, 'Please enter Model Name'], unique: true },
	makeId: [{type: ObjectId, ref: 'Make'}],
	created_at: {type: Date, default: Date.now()}
});

module.exports = mongoose.model('Model', modelSchema);