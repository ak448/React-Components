var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var makeSchema = new Schema({
	name: { type: String, required: [true, 'Please enter Make Name'], unique: true },
	created_at: {type: Date, default: new Date()}
});

module.exports = mongoose.model('Make', makeSchema);