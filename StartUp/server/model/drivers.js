var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var driverSchema = new Schema({
  name: { type: String, required: true},
  username: {type: String, unique: true},
  password: String,
  email: String,
  mobile:{ type: Number},
  profile:[{ type: ObjectId, ref:'Profile'}],
  professionalExp:[{type: ObjectId, ref: 'ProfExperience'}],
  comments: String,
  status: {type: Boolean, default: true},
  created_at: {type: Date, default: new Date()}
});

module.exports = mongoose.model('Driver', driverSchema);