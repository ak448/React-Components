var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var Admin = require('./model/adminLogin');
var Login = require('./model/logins');
var Dsignup = require('./model/driverSignup');
var Driver = require('./model/drivers');
var Profile = require('./model/profile');
var ProfExperience = require('./model/professionalExp');
var Owner = require('./model/owners');
var Taxiinfo = require('./model/taxiinfo');

var mongoose = require('mongoose');
var dbconnect = mongoose.connect('mongodb://localhost/ReactNativeApp');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
   //and remove cacheing so we get the most recent comments
    res.setHeader('Cache-Control', 'no-cache');
    next();
});
//    -----------------------app api's -----------------------------------

app.post('/login', function(req, res){
    
    Login.findOne({username: req.body.username}, function(err, data){
      if(!data) {
        res.json({status: false, msg: 'Username is wrong!'});
      } else {
        if(data.password === req.body.password) {
          res.json({status: true, data: data});
        } else {
          res.json({status: false, msg: 'Password is wrong!'});
        }
      }
    });
});

app.post('/drivers', function(req, res){
  var driver = new Driver();
  driver.name = req.body.name;
  driver.email = req.body.email;
  driver.username = req.body.username;
  driver.password = req.body.password;
  driver.mobile = req.body.mobile;
  driver.save(function(err, data){
    if(err){
      res.send(err);
    } else {
      var login = new Login();
      login.username = req.body.username;
      login.password = req.body.password; 
      login.user_type = req.body.selectOne; 
      login.save(function(err, logdata){
        if(!err){
          res.send(data);
        }
      });
    }
  });
});

app.get('/getdrivers', function(req, res){
  Driver.find({status: true}, function(err, data){
    if(!err){
      res.send(data);
    }
  });
});


app.post('/owners', function(req, res){
  var owner = new Owner();
  owner.name = req.body.name;
  owner.username = req.body.username;
  owner.password = req.body.password;
  owner.contact = req.body.mobile;
  owner.email = req.body.email;
//  owner.shift = req.body.shift;
//  owner.photo = req.body.photo;
//  owner.comments = req.body.comments;
  owner.save(function(err, data){
    if(err){
      res.send(err);
    } else {
      var login = new Login();
      login.username = req.body.username;
      login.password = req.body.password; 
      login.user_type = req.body.selectOne; 
      login.save(function(err, logdata){
        if(!err){
          res.send(data);
        }
      });
    }
  });
});

app.get('/getowner', function(req, res){
  Owner.find({status: true}).populate({path: 'taxi_info', model:'Taxiinfo'}).exec(function(err, data){
    if(!err){
      res.json(data);
    }
  });
});

app.post('/taxi', function(req, res){
  var taxiinfo = new Taxiinfo();
  taxiinfo.company = req.body.company;
  taxiinfo.pickup_location = req.body.pickup_location;
  taxiinfo.taxi_number = req.body.taxi_number;
  taxiinfo.taxi_make = req.body.taxi_make;
  taxiinfo.taxi_model = req.body.taxi_model;
  taxiinfo.year_vehicle = req.body.year_vehicle;
  taxiinfo.miles = req.body.miles;
  taxiinfo.capacity = req.body.capacity;
  taxiinfo.handicapped = req.body.handicapped;
  taxiinfo.lease_amount = req.body.lease_amount;
  taxiinfo.full_time_work = req.body.full_time_work;
  taxiinfo.lat = req.body.lat;
  taxiinfo.lang = req.body.lang;
  taxiinfo.save(function(err, data){
    if(!err) {
      res.json(data);
    }
  });
});

app.post('/driversignup', function(req, res) {
    var driverData = new Dsignup();
    driverData.name = req.body.name;
    driverData.mobileNo = req.body.mobileNo;
    driverData.address = req.body.address;
    driverData.licenseNo = req.body.licenseNo;
    driverData.expiryDate = req.body.expiryDate;

    driverData.save(function(err, doc){
        if(err) {
            res.send('Something went wrong!');
        }
        res.send(doc);
    });
});

// ----------------------------super admin api's ------------------

app.post('/adminLogin', function(req, res){
  
    Admin.findOne({username: req.body.username}, function(err, data){
      if(!data) {
        res.json({status: false, msg: 'Username is wrong!'});
      } else {
        if(data.password === req.body.password) {
          res.json({status: true, data: data});
        } else {
          res.json({status: false, msg: 'Password is wrong!'});
        }
      }
    });
});

app.get('/getadmindrivers', function(req, res){
  Driver.find({}, function(err, data){
    if(!err){
      res.send(data);
    }
  });
});

app.put('/changedriverstatus', function(req, res){
  var id = req.body.id;
  var status = req.body.status;
  Driver.update({_id: id},{$set:{status: !status}}, function(err, data){
    if(!err){
      res.send(data);
    }
  });
});

app.get('/removedriver/:id', function(req, res){
  var id = String(req.params.id);
  console.log(id);
  Driver.remove({_id: mongoose.Types.ObjectId(id)}, function(err, data){
    //if(!err){
      res.send(data);
    //}
  });
});

app.get('/getadminowners', function(req, res){
  Owner.find({}).populate({path:'taxi_info', model:'Taxiinfo'}).exec(function(err, data){
    if(!err){
      res.send(data);
    }
  });
});

app.put('/changeownerstatus', function(req, res){
  var id = req.body.id;
  var status = req.body.status;
  Owner.update({_id: id},{$set:{status: !status}}, function(err, data){
    if(!err){
      res.send(data);
    }
  });
});

app.get('/removeowner/:id', function(req, res){
  var id = String(req.params.id);
  console.log(id);
  Owner.remove({_id: mongoose.Types.ObjectId(id)}, function(err, data){
    //if(!err){
      res.send(data);
    //}
  });
});

app.listen(5000, function(){
    console.log('Server running at port 5000 .....');
});
