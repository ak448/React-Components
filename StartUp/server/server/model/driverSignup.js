var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DSignup = new Schema({
    name: String,
    mobileNo: Number,
    address: String,
    licenseNo: String,
    expiryDate: String
});

module.exports = mongoose.model('Dsignup', DSignup);