var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var driverProfile = new Schema({
  name: { type: String, required: true},
  email:{ type: String},
  city: String,
  country: String,
  address:String,
  zipcode:Number,
});

module.exports = mongoose.model('Profile', driverProfile);