import React, { Component } from 'react';
import { 
    AppRegistry, 
    
 } from 'react-native';
import App from './app/components/App';
import DriverInfo from './app/components/DriverInfo';
import OwnerInfo from './app/components/OwnerInfo';
import {StackNavigator } from 'react-navigation';

export default class StartUp extends Component {
      render() {
          return (
             <App />
        );
    }
  }
const SimpleApp = StackNavigator({
  Home: { screen: App }, 
  Driver: { screen: DriverInfo }, 
  Owner: { screen: OwnerInfo },
});
AppRegistry.registerComponent('StartUp', () => SimpleApp);
          