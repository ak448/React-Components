import React, {Component} from 'react';
import {
    Text, 
    Image, 
    View, 
    StyleSheet, 
    ScrollView,
    TextInput,
    TouchableOpacity,
} from 'react-native';
import Button from './common/Button';
import InputField from './common/InputField';

class DriverInfo extends Component {
    static navigationOptions = {
        title: 'Driver Information',
    };
    constructor(props){
        super(props);
        this.state = {
            text: '',
            address: '',
            mobile: '',
            license:'',
            expiry:''
        }
    }
    render() {
        const {container,labelText, inputStyle} = styles;
        return (
            <ScrollView 
                scrollEnabled={true}
            style={container}
            >
 

            <View>
               
                <InputField 
                    onChangeFunc={(text)=>this.setState({text})}
                    value={this.state.text}
                    placeholder="Enter Name"
                    keyboard="default"
                />
                <InputField 
                    onChangeFunc={(address)=>this.setState({address})}
                    value={this.state.address}
                    placeholder="Enter Address"
                    keyboard="default"
                />
                <InputField 
                    onChangeFunc={(mobile)=>this.setState({mobile})}
                    value={this.state.mobile}
                    placeholder="Enter Mobile"
                    keyboard="numeric"
                />
                <InputField 
                    onChangeFunc={(license)=>this.setState({license})}
                    value={this.state.license}
                    placeholder="Enter License No"
                    keyboard="default"
                />
                <InputField 
                    onChangeFunc={(expiry)=>this.setState({expiry})}
                    value={this.state.expiry}
                    placeholder="Enter Expiry Date"
                    keyboard="default"
                />
                <Button>Submit</Button>
              </View>
</ScrollView>
        );
    }
}

const styles = {
    inputStyle: {
        color: '#000',
        paddingRight: 5,
        paddingLeft: 5,
        fontSize: 18,
        //lineHeight: 23,
        //flex: 2
    },
    labelText: {
        //flex: 1,
        fontSize: 18,
        //paddingLeft: 20
    },
    container: {
        flex: 1,
        //height: 40,
        padding: 10,
        //justifyContent: 'center',
        backgroundColor: '#fff',
        
    }
}

export default DriverInfo;