import React, { Component } from 'react';
import { 
    AppRegistry, 
    Text, 
    View, 
    StyleSheet, 
    TouchableOpacity,
 } from 'react-native';
import App from './app/components/App';

  export default class StartUp extends Component {
      render() {
          return (
              <App />
        );
    }
  }

  const styles = StyleSheet.create({
     note: {
          position: 'relative',
          padding: 20,
          paddingRight: 100,
          borderBottomWidth: 2,
          borderBottomColor:  '#ededed',
     },
     noteText: {
          paddingLeft: 20,
          borderLeftWidth: 10,
          borderLeftColor: '#ededed',
     },
     noteDelete: {
          position: 'absolute',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#2980b9',
          padding: 10,
          top: 10,
          bottom: 10,
          right: 10,
     },
     noteDeleteText: {
          color: 'white',
     } 
  });

