import React, {Component} from 'react';
import {
    Text, 
    Image, 
    View, 
    StyleSheet, 
    ScrollView,
    TextInput,
    TouchableOpacity,
    Button
} from 'react-native';
import DriverInfo from './DriverInfo';
import OwnerInfo from './OwnerInfo';
import InputField from './common/InputField';
import CameraRollPicker from 'react-native-camera-roll-picker';

class App extends Component {
    constructor() {
        super();
        this.state = {
            search: ''
        }
        this.press = this.press.bind(this);
    }
    press(val){
            this.setState({
                search: val
            });
        }
    selectImage = image => {
        alert(image[0].uri);
    }
    static navigationOptions = {
//        title: 'Information',
//        headerTintColor: 'black',
        header: false

    };
    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                {//<CameraRollPicker callback={this.selectImage} maximum={1} assetType="all" />
                }
                <InputField 
                    placeholder="Search Driver"
                    onChangeFunc={this.press}
                    keyboard="default"
                    value={this.state.search}
                />
                <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={()=>navigate('Driver')} style={styles.noteDelete}>
                  <Text style={styles.noteDeleteText}>Driver</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>navigate('Owner')} style={styles.noteDelete}>
                  <Text style={styles.noteDeleteText}>Taxi Owner</Text>
                </TouchableOpacity>
                </View>
                
              </View>
        );
    }
}

const styles = StyleSheet.create({
      container:{
        flex:1,
        justifyContent:'center',
        backgroundColor: '#f4f4f4'
      },
      header:{
        backgroundColor:'#E91E63',
        alignItems:'center',
        justifyContent:'center',
        borderBottomWidth:10,
        borderBottomColor:'#ddd',
      },
      headerText:{
        color:'white',
        fontSize:18,
        padding:26,
      },
      buttonContainer:{
          alignItems: 'center'
      },
      noteDelete: {
          position: 'relative',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#2980b9',
          padding: 10,
          marginBottom: 20,
          marginLeft: 10,
          marginRight: 10,
          borderRadius: 7
//          top: 10,
//          bottom: 30,
//          right: 10,
     },
     noteDeleteText: {
          color: 'white',
     } 
  });

export default App;