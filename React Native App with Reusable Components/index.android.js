/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
} from 'react-native';
import {StackNavigator} from 'react-navigation';
import App from './App';
import Home from './Home';
import Profile from './Profile';

const Route = StackNavigator({
  DashBoard: { screen: App },
  Home: { screen: Home },
  Profile: { screen: Profile }
})

AppRegistry.registerComponent('TestApp', () => Route);
