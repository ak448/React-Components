import React, {Component} from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';
import {StackNavigator} from 'react-navigation';
import Home from './Home';

class App extends Component {
  static navigationOptions = {
    title: 'DashBoard'
  }
  
  render(){
    const { navigate } = this.props.navigation;
  return(
  <View style={styles.container}>
  <Text style={styles.welcome}>
    Welcome to React Native!
      Aviral Firefight @
  </Text>
  <Button title="GO" onPress={()=>navigate('Home')}></Button>
 </View>
  );
 }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
  
  export default App;