import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Picker
} from 'react-native';

class SelectBox extends Component {
  render() {
    return (
      <View>
      	<Picker
		   selectedValue={this.props.selectedValue}
		   style={styles.selectBox}
		   onValueChange={this.props.onValueChange}>
		   <Picker.Item label="Java" value="java" />
		   <Picker.Item label="JavaScript" value="js" />
		</Picker>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	selectBox: {
		marginRight: 15, 
		marginLeft: 15
	}
});


export default SelectBox;