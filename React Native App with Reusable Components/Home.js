import React, {Component} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import InputField from './InputField'; 
import Button from './Button'; 


class Home extends Component {
  static navigationOptions = {
    title: 'Home'
  }
  constructor(props){
    super(props);
    this.state = {
      name: '',
      email: '',
      mobile: '',
      address: ''
    }
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangeMobile = this.onChangeMobile.bind(this);
    this.onChangeAddress = this.onChangeAddress.bind(this);
  }
  onChangeName = (val) => {
    this.setState(
      {name: val}
    );
  }
  onChangeEmail = (val) => {
    this.setState(
      {email: val}
    );
  }
  onChangeMobile = (val) => {
    this.setState(
      {mobile: val}
    );
  }
  onChangeAddress = (val) => {
    this.setState(
      {address: val}
    );
  }
  saveValue = () => {
    this.props.navigation.navigate('Profile', {data: this.state});
  }
  render() {
    const {navigate} = this.props.navigation;
    const {viewStyle, inputStyle, labelText, button} = styles;
    return(
    <View style={viewStyle}>
      <Text style={labelText}>User Detail Form</Text>
        <InputField 
          onChangeFunc={this.onChangeName}
          keyboard="default"
          value={this.state.name}
          placeholder="Enter Name"
          
        />
        <InputField 
          onChangeFunc={this.onChangeEmail}
          keyboard="email-address"
          value={this.state.email}
          placeholder="Enter Email"
          
        />
        <InputField 
          onChangeFunc={this.onChangeMobile}
          keyboard="numeric"
          value={this.state.mobile}
          placeholder="Enter Mobile"
          
        />
        <InputField 
          onChangeFunc={this.onChangeAddress}
          keyboard="default"
          value={this.state.address}
          placeholder="Enter Address"
          
        />
        
        {
          //<Button title="GO to Profile" onPress={()=>navigate('Profile')}></Button>
        }
        <Button onPress={this.saveValue.bind(this)}>
          Submit
        </Button>
    </View>
    );
  }
}

const styles = {
  viewStyle: {
    
  },
  labelText: {
    fontSize: 22,
    paddingLeft: 100,
    paddingTop: 10,
    paddingBottom: 5,
    color: '#444'
  },
  button: {
    backgroundColor: 'skyblue',
    color: 'white',
    height: 40,
    lineHeight: 40,
    marginTop: 10,
    textAlign: 'center',
    
  }
}

export default Home;