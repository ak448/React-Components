import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';

class Profile extends Component {
  static navigationOptions = {
    title: 'Profile'
  }
  render() {
    const {params} = this.props.navigation.state;
    const {viewStyle} = styles;
    return(
    <View style={viewStyle}>
      <Text>Profile Here.</Text>  
      <Text>{params.data.name}</Text>  
    </View>
    );
  }
}

const styles = {
  viewStyle: {
    alignItems: 'center'
  }
}

export default Profile;