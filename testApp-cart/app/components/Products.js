import React, { Component } from 'react';
import { View, Text, Button, TouchableOpacity } from 'react-native';
import ItemList from './ListItem';
import CheckOut from './CheckOut';

const image1 = require('../images/orange.jpg');
const image2 = require('../images/tomato.jpg');
const image3 = require('../images/salmon.jpg');
const image4 = require('../images/greens.jpg');
const image5 = require('../images/rye-bread.jpg');



const data = [
{
  id: 1,
  image: image1,
  name: 'Orange',
  price: 10,
  amountTaken: 3
}, {
  id: 2,
  image: image2,
  name: 'Tomato',
  price: 5,
  amountTaken: 4
}, {
  id: 3,
  image: image3,
  name: 'Salmon fillet',
  price: 16,
  amountTaken: 2
}, {
  id: 4,
  image: image4,
  name: 'Greens',
  price: 3,
  amountTaken: 3
}, {
  id: 5,
  image: image5,
  name: 'Rye Bread',
  price: 20,
  amountTaken: 1
},  
];

class Products extends Component {
    constructor(props){
        super(props);
        this.state = {
            obj: [],
        }
        this.increaseItem = this.increaseItem.bind(this);
        this.decreaseItem = this.decreaseItem.bind(this);
        this._getBasket = this._getBasket.bind(this);
        
        //this.checkOutItems = this.checkOutItems.bind(this);
    }
    static navigationOptions = ({ navigation }) => ({
    title: ` ${navigation.state.params.title}`,
  });
    increaseItem() {
        this.setState({
             count: this.state.count + 1
        })
        
    }
    decreaseItem() {
        if (this.state.count <= 0) {
            this.setState({
                count: 0
            })
        } else {
            this.setState({
                count: this.state.count - 1
            })
        }
    }

_getBasket(item){
    var tempArr = this.state.obj.slice();
    tempArr.push(item);
    this.setState({obj: tempArr});
}
    
  render() {
      const { buttonStyle, textStyle } = styles;
      const { navigate } = this.props.navigation;
      const { params } = this.props.navigation.state;
    return (
        <View>
            { data.map((item, index)=>{
                 return(
                    <ItemList 
                        items={item}
                        key={item.id}
                        checkOut={this.checkOutItems}
                        getBasket = {this._getBasket}
                    />
            );
                }) 
             } 
            
            <TouchableOpacity 
                onPress={()=>{

                    navigate('CheckOut', {items: this.state.obj})
                }} 
                style={buttonStyle} >
                <Text style={textStyle}>Checkout</Text>
            </TouchableOpacity>
            
        </View>

            
    );
  }
}

const styles = {
    textStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 16,
        fontWeight: 'bold',
        paddingTop: 10,
        paddingBottom: 10
    },
    buttonStyle: {
        //flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 5,
        marginRight: 5
    },
}

export default Products;