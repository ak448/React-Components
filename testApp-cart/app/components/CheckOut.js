import React , { Component } from 'react';
import { View, Text } from 'react-native';

class CheckOut extends Component {
    static navigationOptions = ({ navigation }) => ({
    title: `Checkout`,
  });

    render() {
        const { params } = this.props.navigation.state;
        return (
            <View >
            <View style={{ flexDirection:'row', paddingRight: 15, paddingLeft: 15, justifyContent:'space-between', backgroundColor: 'cyan'}}>
                <Text>Name</Text>
                <Text>Qty</Text>
                <Text>Price</Text>
                <Text>Total</Text> 
            </View>
           
                {   
                    params.items.map((item, i)=>{
                        return (<View style={{ flexDirection:'row', paddingRight: 15, paddingLeft: 15, justifyContent:'space-between'}} key={i}>
                            <Text >{item.name}</Text>
                            <Text >{item.amountTaken}</Text>
                            <Text>{item.price}</Text>
                            <Text >{item.amountTaken*item.price}</Text>
                            </View>);
                    })
                }
               
            </View>
        );
    }
}

export default CheckOut;