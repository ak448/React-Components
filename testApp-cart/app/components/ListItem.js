import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
const image1 = require('../images/orange.jpg');
const image2 = require('../images/tomato.jpg');
const image3 = require('../images/salmon.jpg');
const image4 = require('../images/greens.jpg');
const image5 = require('../images/rye-bread.jpg');

class ItemList extends Component {
    constructor (props) {
        super(props);
        //let items = [];
        this.state = {
            amountTaken: this.props.items.amountTaken,
            items: [],
            //obj:[]
        }
        this.addItem = this.addItem.bind(this);
    }
    increaseItemQty() {
       this.setState({
           amountTaken: this.state.amountTaken+1
       })
        //console.log(this.state.count);
        //this.props.checkOut(this.state.count);
    }
    decreaseItemQty() {
       if (this.state.amountTaken <= 0) {
            this.setState({
                amountTaken: 0
            })
        } else {
            this.setState({
                amountTaken: this.state.amountTaken - 1
            })
        }
    }
    addItem() {
        console.log(this.props.items.name);
        this.props.getBasket({
                   name: this.props.items.name, 
                   amountTaken: this.state.amountTaken, 
                   id: this.props.items.id, 
                   price: this.props.items.price});
    }

    render() {
        const { name, image, price, amountTaken, id } = this.props.items;
       
        //console.log(this.props);
        const { 
          imageStyle, 
          textStyle, 
          counterStyle,
          priceStyle } = styles;
        return (
            //<View><Text>List</Text></View>
            <View 
                style={{flexDirection:'row', paddingRight: 15, paddingLeft: 15, justifyContent:'space-between'}} 
                
            >
                <Image source={image} style={styles.imageStyle} />

                <View>
                    <Text style={{ color: '#2e2f30' }}>{name}</Text>

                    <Text style={{ color: '#2e2f30', fontSize: 12 }}>${price}</Text>

                </View>
                <TouchableOpacity 
                    style={{ borderRadius: 15,justifyContent: 'center', alignItems:'center', backgroundColor: '#bbb', height: 30, width: 30,marginTop: 3 }}  
                    onPress={this.decreaseItemQty.bind(this)}
                >
                    <Text style={{fontWeight:'bold'}}>-</Text>
                </TouchableOpacity>
                <Text style={{marginTop: 5}}>{this.state.amountTaken}</Text>
                <TouchableOpacity 
                    onPress={(index)=>this.increaseItemQty()} 
                    style={{ borderRadius: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: '#bbb', height: 30, width: 30, marginTop: 3 }}
                >
                    <Text style={{fontWeight:'bold'}}>+</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={{ borderRadius: 2, justifyContent: 'center', alignItems: 'center', backgroundColor: 'cyan', height:30, width: 40, marginTop: 3, paddingLeft: 5, paddingRight:5 }}
                    onPress={this.addItem}
                ><Text>Add</Text></TouchableOpacity>
          </View>
        );
    }
}

const styles = {
  
  imageStyle: {
    width: 50, 
    height: 50, 
    marginRight: 20
  },
  
  priceStyle: {
    backgroundColor: '#ddd',
    width: 40,
    alignItems: 'center',
    marginTop: 3,
    borderRadius: 3
  },
  counterStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  }
};


export default ItemList;