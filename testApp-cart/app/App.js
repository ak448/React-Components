import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Products from './components/Products';
import ListItem from './components/ListItem';
import CheckOut from './components/CheckOut';

 class TestApp extends Component {
  static navigationOptions = {
    title:'Welcome Shoping Mall'
  }
  render() {
      const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to Shoping!
        </Text>
        <Button title="Goto Products Page" onPress={()=>navigate('Products', {title: 'Products'})} />
        
      </View>
    );
  }
}

export const stackNavigator = StackNavigator({
    Home: { screen: TestApp },
    Products: { screen: Products },
    ListItem: { screen: ListItem },
    CheckOut: { screen: CheckOut }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});